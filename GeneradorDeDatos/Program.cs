﻿using DALInflux;
using DALMongo;

namespace GeneradorDeDatos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generador de datos");
            Console.WriteLine(DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'"));
            RepositorioLecturas lecturas = new RepositorioLecturas();
            RepositoryDispositivos dispositivos = new RepositoryDispositivos();
            DateTime inicio = DateTime.Now, fin;

            Console.Write("Desea generar los dispositivos u obtener los que existen de la BD [1:Generar / 2:Obtener]: ");
            string opcion = Console.ReadLine();
            Console.Write("Cantidad de lecturas a generar por dispositivo: ");
            int cantidad = int.Parse(Console.ReadLine());
            Console.WriteLine("Proceso iniciado a las: " + DateTime.Now.ToLongTimeString());
            Random rand = new Random(DateTime.Now.Second);
            if (opcion == "1")
            {
                Console.WriteLine("Generando 10 dispositivos");
                Dispositivo d;
                for (int i = 0; i < 10; i++)
                {
                    d = dispositivos.Insertar(new Dispositivo()
                    {
                        FechaColocacion = DateTime.Now,
                        Modelo = "Pruebas01",
                        Ubicacion = "Cuarto" + (i + 1).ToString()
                    });
                }
            }

            Console.WriteLine("Obteniendo los dispositivos de la Base de datos...");
            var lstDispositivos = dispositivos.ObtenerTodos;
            Console.WriteLine($"Se obtuvieron {lstDispositivos.Count()} dispositivos");
            foreach (var item in lstDispositivos)
            {
                Console.WriteLine("Generando lecturas del dispositivo: " + item.Ubicacion);
                for (int i = 0; i < cantidad; i++)
                {
                    if (lecturas.Insertar(new Lectura()
                    {
                        Ubicacion = item.Ubicacion,
                        Humedad = rand.Next(10, 60),
                        Temperatura = rand.Next(5, 45)
                    }) == null)
                    {
                        Console.WriteLine("\nError: " + lecturas.Error);
                    }
                    Console.Write(".");
                    Thread.Sleep(500);
                }
                Console.WriteLine();
            }

            fin = DateTime.Now;

            Console.WriteLine($"Proceso terminado a las {fin.ToLongTimeString()} con una duración de {(fin - inicio).TotalSeconds} segundos.");

            Console.ReadLine();

        }
    }
}