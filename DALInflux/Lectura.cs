﻿namespace DALInflux
{
    public class Lectura
    {
        public DateTime FechaHora { get; set; }
        public string Ubicacion { get; set; }
        public double Temperatura { get; set; }
        public double Humedad { get; set; }
    }
}

