﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALInflux
{
    public class DatoLectura
    {
        public DateTime FechaHora { get; set; }
        public string Medicion { get; set; }
        public string Ubicacion { get; set; }
        public string Elemento { get; set; }
        public int Valor { get; set; }

        public DatoLectura(DateTime fechaHora, string medicion, string ubicacion, string elemento, string valor)
        {
            FechaHora = fechaHora;
            Medicion = medicion;
            Ubicacion = ubicacion;
            Elemento = elemento;
            Valor = int.Parse(valor);
        }
    }
}
