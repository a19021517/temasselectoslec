﻿using DALInflux;
using DALMongo;
using Syncfusion.UI.Xaml.Charts;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TermohigrometroDesktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RepositorioLecturas repositorioLecturas;
        RepositoryDispositivos repositoryDispositivos;
        List<Dispositivo> dispositivos;
        Dispositivo dispositivo;
        bool esEdicion = false;
        Random rand;
        public MainWindow()
        {
            InitializeComponent();
            repositorioLecturas = new RepositorioLecturas();
            repositoryDispositivos = new RepositoryDispositivos();
            dispositivos = repositoryDispositivos.ObtenerTodos.ToList();
            ActualizarTabla();
            dispositivo = new Dispositivo();
            wrapDispositivo.DataContext = dispositivo;
            rand = new Random(DateTime.Now.Second);
        }

        private void LlenarCombos()
        {
            cmbUbicacion.Items.Clear();

            foreach (var item in dispositivos)
            {
                cmbUbicacion.Items.Add(item.Ubicacion);
            }

            cmbElemento.Items.Add("Temperatura");
            cmbElemento.Items.Add("Humedad");
        }

        private void ActualizarTabla()
        {
            dtgDispositivos.ItemsSource = null;
            dtgDispositivos.ItemsSource = dispositivos;
            dispositivo = new Dispositivo();
            HabilitarBotones(false);
            GenerarControles();
            LlenarCombos();
        }

        private void GenerarControles()
        {
            wrapLecturas.Children.Clear();
            foreach (var item in dispositivos)
            {
                wrapLecturas.Children.Add(new ControlLectura(item.Ubicacion, repositorioLecturas, 30));
            }
        }

        private void HabilitarBotones(bool v)
        {
            wrapDispositivo.IsEnabled = v;
            btnNuevo.IsEnabled = !v;
            btnEditar.IsEnabled = !v;
            btnCancelar.IsEnabled = v;
            btnGuardar.IsEnabled = v;
            btnEliminar.IsEnabled = !v;
        }



        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            if (dtpDesde.SelectedDate == null || dtpHasta.SelectedDate == null)
            {
                MessageBox.Show("Debe seleccionar valores de inicio y de final", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                if (dtpDesde.SelectedDate.Value >= dtpHasta.SelectedDate.Value)
                {
                    MessageBox.Show("El inicio del intervalo debe ser menor que el final", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else
                {


                    if (string.IsNullOrEmpty(cmbUbicacion.Text) || string.IsNullOrEmpty(cmbElemento.Text))
                    {
                        MessageBox.Show("Por favor indique la ubicación y el elemento a buscar", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        var datos = repositorioLecturas.ObtenerElementosEnIntervalo(dtpDesde.SelectedDate.Value, dtpHasta.SelectedDate.Value, cmbUbicacion.Text, cmbElemento.Text);
                        if (datos != null)
                        {

                            DateTimeAxis primaryAxis = new DateTimeAxis();
                            primaryAxis.Header = "FechaHora";
                            NumericalAxis secundaryAxis = new NumericalAxis();
                            secundaryAxis.Header = "Valor";
                            LineSeries serie = new LineSeries()
                            {
                                ItemsSource = datos,
                                XBindingPath = "FechaHora",
                                YBindingPath = "Valor"
                            };
                            ChartAdornmentInfo info = new ChartAdornmentInfo()
                            {
                                ShowLabel = true,
                                LabelPosition = AdornmentsLabelPosition.Outer

                            };
                            serie.AdornmentsInfo = info;
                            chart.Header = "Valores de " + cmbElemento.Text + " para el " + cmbUbicacion.Text;
                            chart.PrimaryAxis = primaryAxis;
                            chart.SecondaryAxis = secundaryAxis;
                            chart.Series.Clear();
                            chart.Series.Add(serie);
                        }
                        else
                        {
                            MessageBox.Show("Error al traer los datos", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esEdicion = false;
            HabilitarBotones(true);
            dispositivo = new Dispositivo();
            dispositivo.FechaColocacion = DateTime.Now;
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var dato = dtgDispositivos.SelectedItem as Dispositivo;
            if (dato != null)
            {
                esEdicion = true;
                HabilitarBotones(true);
                wrapDispositivo.DataContext = dato;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            Dispositivo r;
            dispositivo = wrapDispositivo.DataContext as Dispositivo;
            if (esEdicion)
            {
                r = repositoryDispositivos.Modificar(dispositivo.Id, dispositivo);
            }
            else
            {
                r = repositoryDispositivos.Insertar(dispositivo);
            }
            if (r != null)
            {
                MessageBox.Show("Dispositivo guardado correctamente", "Dispositivos", MessageBoxButton.OK, MessageBoxImage.Information);
                ActualizarTabla();


            }
            else
            {
                MessageBox.Show("Error al guardar el dispositivo", "Dispositivos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarBotones(false);
            dispositivo = null;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var dato = dtgDispositivos.SelectedItem as Dispositivo;
            if (dato != null)
            {
                if (MessageBox.Show($"¿Realmente deseas eliminar el dispositivo en la ubicación {dato.Ubicacion}?", "Dispositivos", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (repositoryDispositivos.Eliminar(dato.Id))
                    {
                        ActualizarTabla();
                    }
                }
            }
        }

        private void btnBuscarDispositivo_Click(object sender, RoutedEventArgs e)
        {
            var datos = dispositivos.Where(d => d.Ubicacion.ToLower().Contains(txbBuscar.Text.ToLower()));
            dtgDispositivos.ItemsSource = null;
            dtgDispositivos.ItemsSource = datos;
        }

        private void btnRestautar_Click(object sender, RoutedEventArgs e)
        {
            dtgDispositivos.ItemsSource = null;
            dtgDispositivos.ItemsSource = dispositivos;
        }

        private void btnGenerar_Click(object sender, RoutedEventArgs e)
        {
            var dato = dtgDispositivos.SelectedItem as Dispositivo;
            if (dato != null)
            {
                if (repositorioLecturas.Insertar(new Lectura()
                {
                    FechaHora = DateTime.Now,
                    Humedad = rand.Next(0, 100),
                    Temperatura = rand.Next(0, 40),
                    Ubicacion = dato.Ubicacion
                }) != null)
                {
                    MessageBox.Show("Lectura almacenada...", "Lectura aleatoria", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show(repositorioLecturas.Error, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
    }
}
